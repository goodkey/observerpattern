package at.badkey.overserpattern.pattern;

public interface Observer {
	
	public void addItem(Observable o);
	public void informAll();
	
}
