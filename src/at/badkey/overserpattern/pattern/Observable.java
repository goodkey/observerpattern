package at.badkey.overserpattern.pattern;

public interface Observable {

	public String inform();
}
