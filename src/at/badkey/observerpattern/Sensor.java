package at.badkey.observerpattern;

import java.util.ArrayList;
import java.util.List;

import at.badkey.overserpattern.pattern.Observable;
import at.badkey.overserpattern.pattern.Observer;

public class Sensor implements Observer{
	
	private List<Observable> observables = new ArrayList<Observable>();

	@Override
	public void addItem(Observable o) {
		this.observables.add(o);
		
	}

	@Override
	public void informAll() {
		for(Observable o : this.observables) {
			System.out.println(o.inform());
		}
		
	}

}
