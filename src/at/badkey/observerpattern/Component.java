package at.badkey.observerpattern;

public interface Component {
	
	public boolean isAlive();
	public void start();

}
